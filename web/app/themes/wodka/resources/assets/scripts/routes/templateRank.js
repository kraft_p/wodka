import noUiSlider from "nouislider";

export default {
  init() {
    var rangeSliders = document.querySelectorAll('.range');
    var rangeSliderCount = rangeSliders.length;
    var pointsSumNode = document.querySelector('.points-sum');
    var submitRateButton = document.querySelector('.submit-rate');
    var pointsSum;

    function disableRangeSliders() {
      for (var i = 0; i < rangeSliderCount; i++) {
        rangeSliders[i].setAttribute('disabled', true);
      }
    }

    if (submitRateButton) {
      jQuery(submitRateButton).on('click', function() {

        jQuery.ajax({
          url : window.ajaxProperties.ajaxUrl,
          type : 'post',
          data : {
            action : 'rate_product',
            product_id: parseInt(window.location.search.replace('?id=', '')),
            cena: document.querySelector('input[name="price"]').value,
            zapach: document.querySelector('.range--zapach .noUi-handle').getAttribute('aria-valuetext'),
            pierwszy_smak: document.querySelector('.range--pierwszy-smak .noUi-handle').getAttribute('aria-valuetext'),
            smak: document.querySelector('.range--smak .noUi-handle').getAttribute('aria-valuetext'),
            posmak: document.querySelector('.range--posmak .noUi-handle').getAttribute('aria-valuetext'),
            prezentacja_opakowania: document.querySelector('.range--prezentacja-opakowania .noUi-handle').getAttribute('aria-valuetext'),
            pijalnosc: document.querySelector('.range--pijalnosc .noUi-handle').getAttribute('aria-valuetext'),
            suma_punktow: document.querySelector('.points-sum').innerText,
            opinia: document.querySelector('textarea[name="opinion"]').value,
          },
          success: function () {
            jQuery(submitRateButton).replaceWith('<strong>Ocena zapisana</strong>');
            disableRangeSliders();
          },
        });
      });
    } else {
      disableRangeSliders();
    }
    
    function sumCalculate () {
      pointsSum = 0;

      for (var i = 0; i < rangeSliderCount; i++) {
        var value = parseInt(rangeSliders[i].querySelector('.noUi-handle').getAttribute('aria-valuetext'));
        pointsSum = pointsSum + value;
      }

      pointsSumNode.innerText = pointsSum;
    }

    for (var i = 0; i < rangeSliderCount; i++) {
      noUiSlider.create(rangeSliders[i], {
        range: {
          'min': 0,
          'max': 10,
        },
        step: 1,
        start: [5],
        behaviour: 'tap-snap',
        pips: {
          mode: 'steps',
          density: 10,
        },
      });

      rangeSliders[i].noUiSlider.on('end', sumCalculate);
    }
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
