import Chart from 'chart.js';
import SimpleScrollbar from 'simple-scrollbar';

export default {
  init() {
    var data = [];
    var ratesCount = document.querySelector('.ratesCount').getAttribute('data-value');
    var rates = document.querySelectorAll('.rate');
    
    for (var i = 0; i < rates.length; i++) {
      data.push(parseInt(rates[i].getAttribute('data-rate')) / ratesCount);
    }

    var ctx = document.getElementById('chart').getContext('2d');
    new Chart(ctx, {
        type: 'radar',
        data: {
          labels: ['ZAPACH', 'PIERWSZY SAMK', 'SMAK', 'POSMAK', 'PREZENTACJA', 'PIJALNOŚĆ'],
          datasets: [{
              data: data,
              pointBackgroundColor: '#383838',
              backgroundColor: 'rgba(131,196,214,0.5)',
              borderColor: '#383838',
              borderWidth: 2,
          }],
        },
        options: {
          tooltips: {
            enabled: false,  
          },
          scale: {
            ticks: {
              beginAtZero: true,
              min: 0,
              max: 10,
              stepSize: 1,
            },
            pointLabels: {
              fontSize: 14,
              fontColor: '#383838',
              margin: 10,
            },
          },
          legend: {
            display: false,
          },
        },
    });

    var fieldsSum = 0;
    for (var j = 0; j < data.length; j++) {
      var a = data[j];
      var b = data[j + 1] || data[0];

      if (!!a === true && !!b === true) {
        var field = 0.5 * a * b * (Math.sqrt(3) / 2);
        fieldsSum = fieldsSum + field;
      }
    }

    document.querySelector('.recommendation-field span').innerHTML = parseFloat(fieldsSum).toFixed(2);

    SimpleScrollbar.initEl(document.querySelector('.opinions'));
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
