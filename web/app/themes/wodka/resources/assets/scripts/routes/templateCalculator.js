export default {
  init() {
    var calculateButton = document.querySelector('.button.calculate');

    jQuery(calculateButton).on('click', function() {
      jQuery.ajax({
        url : window.ajaxProperties.ajaxUrl,
        type : 'post',
        async: false,
        data : {
          action : 'calculate_wedding_cost',
          product_id: document.querySelector('select[name="product"]').value,
        },
        success: function (response) {
          var price = response;
          var daysNumber = document.querySelector('select[name="daysNumber"]').value;
          var questsNumber = document.querySelector('input[name="guestsNumber"]').value;

          if (price && daysNumber && questsNumber) {
            var bottlesNumber = (parseInt(daysNumber) === 1 ? 0.5 : 0.75) * parseInt(questsNumber);

            document.querySelector('.bottlesNumber').innerHTML = bottlesNumber;
            document.querySelector('.cost').innerHTML = bottlesNumber * price;
            document.querySelector('.results').style.display = '';
          }
        },
      });
    });
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
