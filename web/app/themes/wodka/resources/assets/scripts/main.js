// import external dependencies
import 'jquery';

// Import everything from autoload
import "./autoload/**/*"

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import templateRank from './routes/templateRank';
import templateProduct from './routes/templateProduct';
import templateCalculator from './routes/templateCalculator';

/** Populate Router instance with DOM routes */
const routes = new Router({
  common,
  home,
  templateRank,
  templateProduct,
  templateCalculator,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
