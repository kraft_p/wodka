<header class="header">
  <div class="container">
    <div class="header__background" @if(get_field('header_background')) style="background-image: url({{ the_field('header_background') }})" @endif></div>
    <a class="header__title" href="{{ home_url('/') }}" title="{{ get_bloginfo('name', 'display') }}"><h1>{{ get_bloginfo('name', 'display') }}</h1></a>
    <nav class="nav">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </nav>
    @if(get_field('header_text'))
      <div class="header__text">
        {{ the_field('header_text') }}
      </div>
    @endif
    
    <img src="@asset('images/stars.png')" class="stars" />
  </div>
</header>
