<footer>
  <div class="footer__background" @if(get_field('footer_background')) style="background-image: url({{ the_field('footer_background') }})" @endif></div>
  <div class="container">
    <a class="brand" href="{{ home_url('/') }}"><h5>jakawodkanawesele.pl</h5></a>
  </div>
</footer>
