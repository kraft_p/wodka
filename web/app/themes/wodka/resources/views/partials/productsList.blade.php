@php
$args = array(
  'post_type' => 'page',
  'post_status' => 'publish',
  'orderby' => 'meta_value_num',
  'meta_key' => 'suma_punktow',
  'nopaging' => true
);
$query = new WP_Query($args);
@endphp

<ul class="products-list">
    @if ($query->have_posts())
        @php $index = 0; @endphp

        @while ( $query->have_posts() ) @php $query->the_post() @endphp
        @php
            $index++;
            $ratingSum = 0;
            $price = 0;

            $meta_tag = get_post_meta(get_the_ID());

            if (isset($meta_tag)) {
                if (isset($meta_tag['suma_punktow'])) {
                    $ratingSum = $meta_tag['suma_punktow'][0];
                }

                if (isset($meta_tag['srednia_cena'])) {
                    $price = $meta_tag['srednia_cena'][0];
                }
            }

            $comments = wp_count_comments(get_the_ID());
            @endphp

            <li class="product">
                <span class="rank rank-{{ $index }}">{{ $index }}.</span>
                @if ($index === 1)
                    <img src="@asset('images/rank-1.png')" class="laurel" />
                @elseif ($index === 2)
                    <img src="@asset('images/rank-2.png')" class="laurel" />
                @elseif ($index === 3)
                    <img src="@asset('images/rank-3.png')" class="laurel" />
                @endif
                <a href="{{ the_permalink(get_the_ID()) }}" title="{{ the_title() }}">
                    <div class="product__image-container">
                        @php
                            $image = get_field('bottle_photo', get_the_ID());

                            if (isset($image)) {
                                $thumb = wp_get_attachment_image_src($image, 'medium');

                                if (isset($thumb)) {
                                    $thumbSrc= $thumb[0];
                                }
                            }
                        @endphp
                        @isset($thumbSrc)<img src="{{ $thumbSrc }}" class="product__image"/>@endisset
                    </div>
                </a>
                <a href="{{ the_permalink(get_the_ID()) }}" title="Info o produkcie" class="button button--product-info">Info o produkcie</a>
                <div class="points">Suma pkt. - <span class="value">{{ $ratingSum }}</span></div>
                <div class="reviewsNumber">Opinie - <span class="value">{{ $comments->total_comments }}</span></div>
                <div class="averagePrice">Średnia cena za 0.5l - <span class="value">{{ $price }} zł</span></div>
                <a href="ocen-produkt/?id={{ get_the_ID() }}" title="Oceń" class="button button--black submit-rate">Oceń</a>
            </li>
        @endwhile

        @php wp_reset_postdata(); @endphp
    @endif
</ul>