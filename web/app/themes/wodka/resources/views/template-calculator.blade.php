{{--
  Template Name: Calculator Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <h2>{{ the_title() }}</h2>
    <div class="info">
      @include('partials.content-page')
    </div>

    <h3 class="label">1. Wybierz wódkę</h3>
    @php
    $args = array(
      'meta_key' => '_wp_page_template',
      'meta_value' => 'views/template-product.blade.php'
    );
    $pages = get_pages($args);
    @endphp
    <select name="product" class="select">
      @foreach($pages as $page)
        <option value="{{ $page->ID }}">{{ $page->post_title }}</option>
      @endforeach
    </select>

    <h3 class="label">2. Podaj liczbę gości</h3>
    <input type="text" name="guestsNumber" class="input">

    <h3 class="label">3. Wybierz liczbę dni</h3>
    <select name="daysNumber" class="select">
      <option value="1">1</option>
      <option value="2">2</option>
    </select>

    </br>

    <button class="button button--black calculate">Przelicz</button>

    <div class="results" style="display: none;">
      <p>Na swoje wesele będziesz potrzebować<span class="bottlesNumber rounded"></span>butelek wódki.</p>
      <p>Szacowana cena to<span class="cost rounded"></span> zł.</p>
    </div>

    <img src="@asset('images/rings-2.png')" class="rings" />
  @endwhile
@endsection