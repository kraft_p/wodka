{{--
  Template Name: Front Page Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  	@include('partials.productsList')

		<img src="@asset('images/rings.png')" class="rings" />
  @endwhile
@endsection
