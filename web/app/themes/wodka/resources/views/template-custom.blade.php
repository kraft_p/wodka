{{--
  Template Name: Custom Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <h2>{{ the_title() }}</h2>
    @include('partials.content-page')
  @endwhile
@endsection
