{{--
  Template Name: Guide Template
--}}

@extends('layouts.app')

@section('content')
	@while(have_posts()) @php the_post() @endphp
		<div class="container">
			<h2>{{ the_title() }}</h2>
		</div>

    @if(have_rows('block'))
			@while(have_rows('block')) @php the_row() @endphp
				<div class="block" @if(get_sub_field('block_background')) style="background-color: #ececec;" @endif>
					@if(get_sub_field('block_icon'))
						<img src="{{ the_sub_field('block_icon') }}" class="block__icon" />
					@endif
					@if(get_sub_field('block_text'))
						<div class="block__text container">
							@if(get_sub_field('block_header'))
								<h3 class="block__header">{{ the_sub_field('block_header') }}</h3>
							@endif
							{{ the_sub_field('block_text') }}
						</div>
					@endif
					@if(get_sub_field('block_image'))
						<div class="block__image" style="background-image: url({{ the_sub_field('block_image') }});"></div>
					@endif
				</div>
			@endwhile
    @endif
  @endwhile
@endsection
