{{--
  Template Name: Rank Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @php
		global $wp_query;
		if (isset($wp_query->query_vars['id'])) { $page = get_page($wp_query->query_vars['id']); }
    @endphp
        
    @if (isset($page))
		<div class="row">
			<div class="col">
				<h2>OCEŃ PRODUKT</h2>

				<h3 class="label">CENA</h3>
				<input type="text" name="price" placeholder="__,__" class="input">

				<p class="info">Podaj cenę wódki.</p>
				<p class="info">Degustacji dokonujemy w temperaturze od 2<sup>o</sup>C do 4<sup>o</sup>C.</p>

				<h3 class="label">ZAPACH</h3>
				<div class="ranges">
					<span>Ostry</span>
					<span>Neutralny</span>
				</div>
				<div class="range range--zapach"></div>

				<h3 class="label">PIERWSZY SMAK</h3>
				<div class="ranges">
					<span>Wstrząsający</span>
					<span>Gładki</span>
				</div>
				<div class="range range--pierwszy-smak"></div>

				<h3 class="label">SMAK</h3>
				<div class="ranges">
					<span>Ostry</span>
					<span>Delikatny</span>
				</div>
				<div class="range range--smak"></div>

				<h3 class="label">PO SMAK</h3>
				<div class="ranges">
					<span>Alkoholowy</span>
					<span>Neutralny</span>
				</div>
				<div class="range range--posmak"></div>

				<h3 class="label">PREZENTACJA OPAKOWANIA</h3>
				<div class="ranges">
					<span>Niska</span>
					<span>Wysoka</span>
				</div>
				<div class="range range--prezentacja-opakowania"></div>

				<h3 class="label">PIJALNOŚĆ</h3>
				<div class="ranges">
					<span>Wstrząsa</span>
					<span>Dobrze wchodzi</span>
				</div>
				<div class="range range--pijalnosc"></div>

				@if (!(isset($_COOKIE['ratedProduct']) && isset($_COOKIE['ratedProduct'][$wp_query->query_vars['id']])))
				<h3 class="label">DODAJ OPINIĘ</h3>
				<textarea name="opinion" rows="5" cols="60" class="textarea"></textarea>
				@endif
			</div>
			<div class="col">
				<img src="@asset('images/birds.png')" class="birds" />

				@php
				$args = array(
					'post_type' => 'page',
					'orderby'   => 'meta_value_num',
					'meta_key'  => 'suma_punktow',
					'fields' => 'ids',
					'nopaging' => true
				);
				$query = new WP_Query($args);
				$index = array_search($page->ID, $query->posts);
				wp_reset_postdata();
				@endphp

				@if($index > -1)<span class="orderNumber">{{ $index + 1 }}.</span>@endif
				
				@php
					$image = get_field('bottle_photo', $page->ID);

					if (isset($image)) {
						$thumb = wp_get_attachment_image_src($image, 'large');

						if (isset($thumb)) {
							$thumbSrc= $thumb[0];
						}
					}
				@endphp
				@isset($thumb)<img src="{{ $thumbSrc }}" class="bottle"/>@endisset
			</div>
		</div>
		<div class="row">
			<div class="col center">
				@if (isset($_COOKIE['ratedProduct']) && isset($_COOKIE['ratedProduct'][$wp_query->query_vars['id']]))
					<span>Produkt został już oceniony</span></br>
				@else
					<div class="button button--black submit-rate">Potwierdź</div></br>
				@endif

				<div class="button points-sum">30</div></br>
				<span>Suma punktów</span></br>
				<a href="{{ home_url('/') }}" class="button front-page">Strona główna</a>
			</div>
			<div class="col">
				<div class="justify">
					@php
					if ($index - 1 > -1) {
						$prevID = $query->posts[$index - 1];
					}

					if ($index + 1 < count($query->posts)) {
						$nextID = $query->posts[$index + 1];
					}
					@endphp

					<div>
						@isset($prevID)
							<a href="{{ home_url('/') }}ocen-produkt/?id={{ $prevID }}" title="{{ get_the_title($prevID) }}" class="button button--black">Poprzedni produkt</a>
						@endisset
					</div>

					<div>
						@isset($nextID)
							<a href="{{ home_url('/') }}ocen-produkt/?id={{ $nextID }}" title="{{ get_the_title($nextID) }}" class="button button--black">Następny produkt</a>
						@endisset
					</div>
				</div>
			</div>
		</div>
	@else
		@include('partials.productsList');
    @endif
    
  @endwhile
@endsection