{{--
  Template Name: Product Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="row">
      <div class="col center">
        @php
        $args = array(
          'post_type' => 'page',
          'orderby'   => 'meta_value_num',
          'meta_key'  => 'suma_punktow',
          'fields' => 'ids',
          'nopaging' => true
        );
        $query = new WP_Query($args);

        $index = array_search(get_the_ID(), $query->posts);
        wp_reset_postdata();
        @endphp

        <h2>@if($index > -1)<span class="orderNumber">{{ $index + 1 }}.</span>@endif{{ the_title() }}</h2>

        @php
          $image = get_field('bottle_photo');

          if (isset($image)) {
            $thumb = wp_get_attachment_image_src($image, 'medium');

            if (isset($thumb)) {
              $thumbSrc= $thumb[0];
            }
          }
        @endphp
        @isset($thumb)<img src="{{ $thumbSrc }}" class="bottle"/>@endisset

        @php
        $args = array(
          'post_id' => get_the_ID()
        );
        $comments = get_comments($args);
        @endphp

        @if ($comments)
        <h4 class="rounded">Opinie</h4>
        <ul class="opinions">
        @foreach ($comments as $comment)
          <li>{{ $comment->comment_content }}</li>
        @endforeach
        </ul>
        @endif
      </div>
      <div class="col">
        <h4>Producent:</h4>
        {{ the_field('producer') }}

        <h4>Opis produktu:</h4>
        {{ the_field('product_description') }}

        <h4>Ocena degustacji:</h4>
        <span class="ratesCount" data-value="{{ get_post_meta(get_the_ID(), 'licznik_ocen', true) }}"></span>
        <span class="rate" data-rate="{{ get_post_meta(get_the_ID(), 'zapach', true) }}"></span>
        <span class="rate" data-rate="{{ get_post_meta(get_the_ID(), 'pierwszy_smak', true) }}"></span>
        <span class="rate" data-rate="{{ get_post_meta(get_the_ID(), 'smak', true) }}"></span>
        <span class="rate" data-rate="{{ get_post_meta(get_the_ID(), 'posmak', true) }}"></span>
        <span class="rate" data-rate="{{ get_post_meta(get_the_ID(), 'prezentacja_opakowania', true) }}"></span>
        <span class="rate" data-rate="{{ get_post_meta(get_the_ID(), 'pijalnosc', true) }}"></span>
        <canvas id="chart"></canvas>

        <p class="recommendation-field">POLE REKOMENDACJI<span class="rounded">0</span></p>
      </div>
    </div>
    <div class="row">
      <div class="col center">
        <a href="{{ home_url('/') }}" class="button front-page">Strona główna</a>
      </div>
      <div class="col justify">
        @php
          if ($index - 1 > -1) {
            $prevID = $query->posts[$index - 1];
          }

          if ($index + 1 < count($query->posts)) {
            $nextID = $query->posts[$index + 1];
          }
        @endphp

        <div>
        @isset($prevID)
          <a href="{{ get_permalink($prevID) }}" title="{{ get_the_title($prevID) }}" class="button button--black">Poprzedni produkt</a>
        @endisset
        </div>

        <div>
        @isset($nextID)
          <a href="{{ get_permalink($nextID) }}" title="{{ get_the_title($nextID) }}" class="button button--black">Następny produkt</a>
        @endisset
        </div>
      </div>
    </div>

    <img src="@asset('images/bird.png')" class="bird" />
  @endwhile
@endsection
