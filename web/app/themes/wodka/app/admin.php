<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

add_action('save_post', function($post_ID) {
    global $wpdb;
    if (!wp_is_post_revision($post_ID) && get_page_template_slug($post_ID) === 'views/template-product.blade.php') {
        add_post_meta($post_ID, 'zapach', 0, true);
        add_post_meta($post_ID, 'pierwszy_smak', 0, true);
        add_post_meta($post_ID, 'smak', 0, true);
        add_post_meta($post_ID, 'posmak', 0, true);
        add_post_meta($post_ID, 'prezentacja_opakowania', 0, true);
        add_post_meta($post_ID, 'pijalnosc', 0, true);
        add_post_meta($post_ID, 'licznik_ocen', 0, true);
        add_post_meta($post_ID, 'suma_punktow', 0, true);
        add_post_meta($post_ID, 'srednia_cena', 0, true);
    }
});

add_action('post_updated', function($post_ID) {
    global $wpdb;
    if (get_page_template_slug($post_ID) === 'views/template-product.blade.php') {
        $sum = 0;
        $sum = $sum + (int)get_post_meta($post_ID, 'zapach', true);
        $sum = $sum + (int)get_post_meta($post_ID, 'pierwszy_smak', true);
        $sum = $sum + (int)get_post_meta($post_ID, 'smak', true);
        $sum = $sum + (int)get_post_meta($post_ID, 'posmak', true);
        $sum = $sum + (int)get_post_meta($post_ID, 'prezentacja_opakowania', true);
        $sum = $sum + (int)get_post_meta($post_ID, 'pijalnosc', true);
        update_post_meta($post_ID, 'suma_punktow', $sum);
    }
});

add_action('acf/render_field_settings/type=image', function($field) {
    acf_render_field_setting( $field, array(
        'label'			=> 'Default Image',
        'instructions'		=> 'Appears when creating a new post',
        'type'			=> 'image',
        'name'			=> 'default_value',
    ));
});

